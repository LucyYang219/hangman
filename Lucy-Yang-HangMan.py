from turtle import*
from tkinter import messagebox
import random
t=Turtle()
s=Turtle()
screen=t.getscreen()
t.speed(6)
t.hideturtle()
s.hideturtle()
def namechoosing():
    chioces = ["A.L.A.N T.U.R.I.N.G", "D.O.N.A.L.D K.N.U.T.H", "A.D.A L.O.V.E.L.A.C.E", "G.R.A.C.E H.O.P.P.E.R", "G.O.R.D.O.N M.O.O.R.E"]
    rep = random.randint(0, 4) 
    name = chioces[rep]
    return name
def spacing(name):  #makes the dashes 
    def dash(x): 
        s.pu()
        s.goto(x,-50)
        s.pd()
        s.fd(20)
    names=name.split(' ') 
    z=-80
    for i in range(len(names)): 
        words=names[i]
        letters=words.split(".") 
        for q in range(len(letters)):
            dash(z)
            z+=30
        z=z+20
def drawHook():
    t.color("brown")
    t.pensize(10)
    t.pu()
    t.goto(-130,150)
    t.pd()
    t.lt(90)
    t.fd(25)
    t.lt(90)
    t.fd(60)
    t.lt(90)
    t.fd(220)
    t.pu()
    t.goto(-270,-50)
    #t.goto(-290,-50)
    t.pd()
    t.lt(90)
    #t.fd(80)
    t.fd(180)
    t.color("black")
    t.pu()
def updateGuess(insert,index):
    index.append(insert)                              
    return index
def drawGuess(name,insert):
    t.pencolor("black")
#    t.pencolor("deep pink")
    names=name.split(' ') 
    z=-80
    for i in range(len(names)): 
        words=names[i]
        letters=words.split(".") 
        for q in range(len(letters)):
            t.goto(z,-50)
            if insert==letters[q]:
                t.write(insert, font=("Albertus MT", 14, "bold"))
            z+=30
        z=z+20
    t.pencolor("black")
def drawMan(wrong): #moved 70 units down 
    if wrong==1: #head
        t.goto(-130,90)
        t.pd()
        t.circle(30)
    if wrong==2: #body
        t.goto(-130,90)
        t.pd()
        t.rt(90)
        t.fd(100)
    if wrong==3: #left arm
        t.goto(-130,75)
        t.pd()
        t.rt(40)
        t.fd(40)
    if wrong==4: #right arm
        t.goto(-130,75)
        t.pd()
        t.lt(80)
        t.fd(40)
    if wrong==5: #left leg
        t.goto(-130,-10)
        t.pd()
        t.rt(80)
        t.fd(40)
    if wrong==6: #right leg
        t.goto(-130,-10)
        t.pd()
        t.lt(80)
        t.fd(40)
def drawAlphabet(): #writes out the green letters 
    alphabet=["A", "B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
    s.pu()
    x=-250
    for i in range(len(alphabet)):
        s.goto(x,-150)
        s.pencolor("green")
        s.write(alphabet[i], font=("Albertus MT", 14, "bold"))     
        x+=18
def turnLetterRed(guess):
    s.pu()
    x=-250
    s.pencolor("red")
    if guess == "A":
        s.goto(x,-150)
        s.write(guess, font=("Albertus MT", 14, "bold"))
    else:
        x+=18
    if guess == "B":
        s.goto(x,-150)
        s.write(guess, font=("Albertus MT", 14, "bold"))
    else:
        x+=18
    if guess == "C":
        s.goto(x,-150)
        s.write(guess, font=("Albertus MT", 14, "bold"))
    else:
        x+=18
    if guess == "D":
        s.goto(x,-150)
        s.write(guess, font=("Albertus MT", 14, "bold"))
    else:
        x+=18
    if guess == "E":
        s.goto(x,-150)
        s.write(guess, font=("Albertus MT", 14, "bold"))
    else:
        x+=18
    if guess == "F":
        s.goto(x,-150)
        s.write(guess, font=("Albertus MT", 14, "bold"))
    else:
        x+=18
    if guess == "G":
        s.goto(x,-150)
        s.write(guess, font=("Albertus MT", 14, "bold"))
    else:
        x+=18
    if guess == "H":
        s.goto(x,-150)
        s.write(guess, font=("Albertus MT", 14, "bold"))
    else:
        x+=18
    if guess == "I":
        s.goto(x,-150)
        s.write(guess, font=("Albertus MT", 14, "bold"))
    else:
        x+=18
    if guess == "J":
        s.goto(x,-150)
        s.write(guess, font=("Albertus MT", 14, "bold"))
    else:
        x+=18
    if guess == "K":
        s.goto(x,-150)
        s.write(guess, font=("Albertus MT", 14, "bold"))
    else:
        x+=18
    if guess == "L":
        s.goto(x,-150)
        s.write(guess, font=("Albertus MT", 14, "bold"))
    else:
        x+=18
    if guess == "M":
        s.goto(x,-150)
        s.write(guess, font=("Albertus MT", 14, "bold"))
    else:
        x+=18
    if guess == "N":
        s.goto(x,-150)
        s.write(guess, font=("Albertus MT", 14, "bold"))
    else:
        x+=18
    if guess == "O":
        s.goto(x,-150)
        s.write(guess, font=("Albertus MT", 14, "bold"))
    else:
        x+=18
    if guess == "P":
        s.goto(x,-150)
        s.write(guess, font=("Albertus MT", 14, "bold"))
    else:
        x+=18
    if guess == "Q":
        s.goto(x,-150)
        s.write(guess, font=("Albertus MT", 14, "bold"))
    else:
        x+=18
    if guess == "R":
        s.goto(x,-150)
        s.write(guess, font=("Albertus MT", 14, "bold"))
    else:
        x+=18
    if guess == "S":
        s.goto(x,-150)
        s.write(guess, font=("Albertus MT", 14, "bold"))
    else:
        x+=18
    if guess == "T":
        s.goto(x,-150)
        s.write(guess, font=("Albertus MT", 14, "bold"))
    else:
        x+=18
    if guess == "U":
        s.goto(x,-150)
        s.write(guess, font=("Albertus MT", 14, "bold"))
    else:
        x+=18
    if guess == "V":
        s.goto(x,-150)
        s.write(guess, font=("Albertus MT", 14, "bold"))
    else:
        x+=18
    if guess == "W":
        s.goto(x,-150)
        s.write(guess, font=("Albertus MT", 14, "bold"))
    else:
        x+=18
    if guess == "X":
        s.goto(x,-150)
        s.write(guess, font=("Albertus MT", 14, "bold"))
    else:
        x+=18
    if guess == "Y":
        s.goto(x,-150)
        s.write(guess, font=("Albertus MT", 14, "bold"))
    else:
        x+=18
    if guess == "Z":
        s.goto(x,-150)
        s.write(guess, font=("Albertus MT", 14, "bold"))
def drawBlankScreen():
    t.speed(0)
    t.pu()
    t.goto(-320, 300)
    t.pen(fillcolor="white", pencolor="white")
    t.pd()
    t.begin_fill()
    t.fd(650)
    t.rt(90)
    t.fd(600)
    t.rt(90)
    t.fd(650)
    t.rt(90)
    t.fd(600)
    t.end_fill()
    t.pu()
def drawSadFace():
    t.goto(60, 210)
    t.pen(fillcolor="lemon chiffon", pencolor="black", pensize=2.5)
    t.pd()
    t.begin_fill()
    t.circle(30)
    t.end_fill()
    t.pu()
    t.goto(68, 240)
    t.dot(12, "black")
    t.goto(95, 240)
    t.dot(12, "black")
    t.goto(105, 215)
    t.lt(190)
    t.pd()
    t.circle(17, 120)
    t.pu()
    t.goto(130, 200)
    t.write("You lost!" , font=("Comic Sans MS", 30, "bold"))
    t.goto(-100, 170)
    
def main():
    name=namechoosing() #name choosen 
    spacing(name)
    drawHook()
    guessed=[]  #letters guessed 
    wrong=0
    named=[]
    person=[] #name chosen and sorted 
    drawAlphabet() #draws  the green letters 
    guess=screen.textinput("GUESS", "Guess my letters!")
    names=name.split(' ') 
    for i in range(len(names)): 
        words=names[i]
        letters=words.split(".")
        for q in range(len(letters)):
            if letters[q] not in person:
                person.append(letters[q])
    person=sorted(person)
    
    while wrong<6 and named!=person and guess != None:
        if len(guess) == 0:
            messagebox.showinfo("EMPTY INPUT", "Please enter letters")
            guess=screen.textinput("GUESS", "Guess my letters!")
        else:
            guess=guess.upper()
            guess=guess[0] #only accepts the frist letter guessed
            if guess < "A" or guess > "Z":
                messagebox.showinfo("INVAILD INPUT", "Please enter letters only")
                guess=screen.textinput("GUESS", "Guess my letters!")
            elif guess in guessed:
                messagebox.showinfo("GUESSED", "You have already guessed " + guess + ". \nPlease guess from the following letters that are in green.")
                guess=screen.textinput("GUESS", "Guess my letters!")
            else:
                t.pu()
                if guess in name:
                    drawGuess(name,guess)
                    named.append(guess)
                    named=sorted(named)
                else:
                    wrong+=1
                    drawMan(wrong)
                guessed=updateGuess(guess,guessed)
                turnLetterRed(guess)
                if wrong<6 and named!=person:
                        guess=screen.textinput("GUESS", "Guess my letters!")
    t.pu()
    if guess == None:
        drawBlankScreen()
        t.goto(-330, 50)
        t.pencolor("sky blue")
        t.write("Thank You For Playing", font=("Comic Sans MS", 45, "bold"))
        t.goto(-330, -50)
        t.write("We hope you will play again soon!", font=("Comic Sans MS", 30, "bold"))
    t.goto(-300, 200)
    if person == named:
        t.write("CONTRATULATION! YOU WON!", font=("Comic Sans MS", 30, "bold"))
    if wrong==6:
        t.write("WOOMP WOOMP" , font=("Comic Sans MS", 30, "bold"))
        drawSadFace() 
        t.write("The answer was: " + name.replace('.', ''), font=("Comic Sans MS", 15, "bold"))


main()
done()
